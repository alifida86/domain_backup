package com.rocketcase.clioclient.domain;
// default package

// Generated Apr 28, 2017 6:29:29 PM by Hibernate Tools 5.1.0.Alpha1

import static javax.persistence.GenerationType.IDENTITY;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.rocketcase.clioclient.domain.base.BaseEntity;
import com.rocketcase.clioclient.domain.clio.apiv4.Account;

@Entity
@Table(name = "applications")
public class Application implements BaseEntity, java.io.Serializable {

	@Transient
	public static final String SINGLE = "application";
	@Transient
	public static final String URL_PRE_FIX = "application";

	@Transient
	public static final String REFRESH_APPLICATION_CACHE = URL_PRE_FIX + "/refresh/account/cache";

	@Transient
	public static final String LIST_URL = URL_PRE_FIX + "/list";
	@Transient
	public static final String GET_BY_DOMAIN_URL = URL_PRE_FIX + "/domain/";

	@Transient
	public static final String GET_BY_ID_URL = URL_PRE_FIX + "/id/";
	@Transient
	public static final String GET_BY_SECRET_KEY_URL = URL_PRE_FIX + "/secretkey/";
	@Transient
	public static final String SAVE_URL = URL_PRE_FIX + "/save";
	@Transient
	public static final String DELETE_URL = URL_PRE_FIX + "/delete/";

	private static final long serialVersionUID = 1L;
	private Long id;
	private String name;
	private String description;
	private String secretKey;
	private String domain;

	private String clioDetails;
	private Map<String, String> clioDetailsMap;

	private String chargeBee;
	private String chargeBeeStatus;

	private Map<String, String> chargeBeeMap;


	private Package activePackage;

	private List<Account> accounts = new ArrayList<Account>(0);

	private String status;

	public Application() {
	}

	public Application(String domain, String name) {
		this.domain = domain;
		this.name = name;
	}

	public Application(Long id) {
		this.id = id;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "domain")
	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	@Column(name = "name")
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "description")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Column(name = "secret_key")
	public String getSecretKey() {
		return secretKey;
	}

	public void setSecretKey(String secretKey) {
		this.secretKey = secretKey;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "active_package_id")
	public Package getActivePackage() {
		return activePackage;
	}

	public void setActivePackage(Package activePackage) {
		this.activePackage = activePackage;
	}

	@Column(name = "clio_details")
	public String getClioDetails() {
		return clioDetails;
	}

	public void setClioDetails(String clioDetails) {
		this.clioDetails = clioDetails;
	}

	@Transient
	public List<Account> getAccounts() {
		return accounts;
	}

	public void setAccounts(List<Account> accounts) {
		this.accounts = accounts;
	}

	@Column(name = "charge_bee")
	public String getChargeBee() {
		return chargeBee;
	}

	public void setChargeBee(String chargeBee) {
		this.chargeBee = chargeBee;
	}

	@Column(name = "charge_bee_status")
	public String getChargeBeeStatus() {
		return chargeBeeStatus;
	}

	public void setChargeBeeStatus(String chargeBeeStatus) {
		this.chargeBeeStatus = chargeBeeStatus;
	}

	@Transient
	public Map<String, String> getChargeBeeMap() {
		return chargeBeeMap;
	}

	public void setChargeBeeMap(Map<String, String> chargeBeeMap) {
		this.chargeBeeMap = chargeBeeMap;
	}

	@Transient
	public Map<String, String> getClioDetailsMap() {
		return clioDetailsMap;
	}

	public void setClioDetailsMap(Map<String, String> clioDetailsMap) {
		this.clioDetailsMap = clioDetailsMap;
	}

	
}

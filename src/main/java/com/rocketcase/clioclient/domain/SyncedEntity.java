package com.rocketcase.clioclient.domain;
// default package

// Generated Apr 28, 2017 6:29:29 PM by Hibernate Tools 5.1.0.Alpha1

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * TablesSyncedStatus
 */
@Entity
@Table(name = "synced_entities")
public class SyncedEntity implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8270103981112384646L;
	/*private Long id;*/
	private String id;
	private String syncedTimestamp;
	private String description;

	public SyncedEntity() {
	}

	public SyncedEntity( String id, String syncedTimestamp, String description) {
		//this.id=id;
		this.id = id;
		this.syncedTimestamp = syncedTimestamp;
		this.description = description;
	}
	public SyncedEntity(String id, String syncedTimestamp) {
		this.id = id;
		this.syncedTimestamp = syncedTimestamp;
	}

	/*@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}*/
	
	@Id
	@Column(name = "entity", unique = true, nullable = false)
	public String getEntity() {
		return this.id;
	}

	public void setEntity(String id) {
		this.id = id;
	}

	@Column(name = "synced_timestamp")
	public String getSyncedTimestamp() {
		return this.syncedTimestamp;
	}

	public void setSyncedTimestamp(String syncedTimestamp) {
		this.syncedTimestamp = syncedTimestamp;
	}

	public String getDescription() {
		return description;
	}

	@Column(name = "description")
	public void setDescription(String description) {
		this.description = description;
	}

}

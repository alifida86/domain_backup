package com.rocketcase.clioclient.domain;
// default permissionLevel

// Generated Apr 28, 2017 6:29:29 PM by Hibernate Tools 5.1.0.Alpha1

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.rocketcase.clioclient.domain.base.BaseEntity;

@Entity
@Table(name = "permission_levels")
public class PermissionLevel implements BaseEntity, java.io.Serializable {

	@Transient
	public static final String SINGLE = "permissionLevel";
	@Transient
	public static final String URL_PRE_FIX = "permissionLevel";

	@Transient
	public static final String LIST_URL = URL_PRE_FIX + "/list";

	@Transient
	public static final String FREE_URL = URL_PRE_FIX + "/free";

	@Transient
	public static final String GET_BY_APPLICATION_URL = URL_PRE_FIX + "/application/";

	@Transient
	public static final String GET_BY_ID_URL = URL_PRE_FIX + "/id/";

	@Transient
	public static final String SAVE_URL = URL_PRE_FIX + "/save";

	@Transient
	public static final String DELETE_URL = URL_PRE_FIX + "/delete/";

	private static final long serialVersionUID = 1L;
	public static final String GET_BY_NAME_URL = URL_PRE_FIX + "/name/";
	private Long id;
	private String name;

	public PermissionLevel() {
	}

	public PermissionLevel(Long id) {
		this.id = id;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "name")
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

}

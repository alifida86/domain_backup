package com.rocketcase.clioclient.domain;
// default package

// Generated Apr 28, 2017 6:29:29 PM by Hibernate Tools 5.1.0.Alpha1

import static javax.persistence.GenerationType.IDENTITY;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.rocketcase.clioclient.domain.base.BaseEntity;
import com.rocketcase.clioclient.domain.clio.apiv4.Contact;

/**
 * AclUsers
 */
@Entity
@Table(name = "acl_users")
public class User implements BaseEntity, java.io.Serializable {

	@Transient
	public static final String SINGLE = "user";

	@Transient
	public static final String URL_PRE_FIX = "user";

	@Transient
	public static final String LIST_URL = URL_PRE_FIX + "/list";
	
	
	@Transient
	public static final String GET_BY_ID_URL = URL_PRE_FIX + "/id/";

	@Transient
	public static final String GET_BY_IDS_URL = URL_PRE_FIX + "/ids/";
	
	@Transient
	public static final String GET_BY_USER_NAME_URL = URL_PRE_FIX + "/username/";
	@Transient
	public static final String GET_BY_APPLICATION_URL = URL_PRE_FIX + "/application/";

	@Transient
	public static final String GET_BY_FILTER_URL = URL_PRE_FIX + "/search";
	@Transient
	public static final String GET_USER_MAP_BY_FILTER_URL = URL_PRE_FIX + "/usermap/search/";
	@Transient
	public static final String VALIDATE_URL = URL_PRE_FIX + "/validate/";
	@Transient
	public static final String SAVE_URL = URL_PRE_FIX + "/save";
	@Transient
	public static final String DELETE_URL = URL_PRE_FIX + "/delete/";

	@Transient
	public static final String DELETE_BY_IDS_URL = "/deletebyids/";

	@Transient
	public static final String REMOVE_CLIO_DELETED_USERS = URL_PRE_FIX + "/remove/clio/deleted/users/";
	@Transient
	public static final String CREATE_CLIO_CLIENT_USERS = URL_PRE_FIX + "/create/clio/client/users/";
	@Transient
	public static final String CREATE_CLIO_ADMIN_USER = URL_PRE_FIX + "/create/clio/admin/user/";

	/**
	 * 
	 */
	private static final long serialVersionUID = -1611945140787694550L;

	
	private Long id;
	private String loginId;
	private String password;
	private String decryptedPassword;

	private String alternateEmail;
	private String displayName;

	private String timezone;

	private String status;

	private String userStatus;

	private PermissionLevel permissionLevel;
	
	private Date lastLoginAt;

	private ContactDetail contactDetail;

	private List<Group> groups = new ArrayList<Group>(0);
	private List<Permission> permissions = new ArrayList<Permission>(0);
	private List<Application> applications = new ArrayList<Application>(0);

	private Contact client;
	
	public User() {
	}

	public User(Long id) {
		this.id = id;
	}
	public User(String loginId, String password) {
		this.loginId = loginId;
		this.password = password;
	}
	
	public User(String loginId, Long groupId, Long applicationId) {
		this.loginId = loginId;
		applications.add(new Application(applicationId));
		groups.add(new Group(groupId));
	}

	public User(Long id, Long groupId) {

	}

	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "login_id")
	public String getLoginId() {
		return this.loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	@Column(name = "password")
	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Transient
	public List<Group> getGroups() {
		return groups;
	}

	public void setGroups(List<Group> groups) {
		this.groups = groups;
	}

	@Transient
	public String getDecryptedPassword() {
		return decryptedPassword;
	}

	public void setDecryptedPassword(String decryptedPassword) {
		this.decryptedPassword = decryptedPassword;
	}

	@Transient
	public String getAlternateEmail() {
		return alternateEmail;
	}

	public void setAlternateEmail(String alternateEmail) {
		this.alternateEmail = alternateEmail;
	}

	@Transient
	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	@Column(name = "timezone")
	public String getTimezone() {
		return timezone;
	}

	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}

	@Column(name = "status")
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "last_login_at")
	public Date getLastLoginAt() {
		return lastLoginAt;
	}

	public void setLastLoginAt(Date lastLoginAt) {
		this.lastLoginAt = lastLoginAt;
	}

	@Transient
	public List<Permission> getPermissions() {
		return permissions;
	}

	public void setPermissions(List<Permission> permissions) {
		this.permissions = permissions;
	}

	@Transient
	public List<Application> getApplications() {
		return applications;
	}

	public void setApplications(List<Application> applications) {
		this.applications = applications;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "contact_id")
	public ContactDetail getContactDetail() {
		return contactDetail;
	}

	public void setContactDetail(ContactDetail contactDetail) {
		this.contactDetail = contactDetail;
	}

	
	@Column(name="user_status")
	public String getUserStatus() {
		return userStatus;
	}

	public void setUserStatus(String userStatus) {
		this.userStatus = userStatus;
	}

	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "permission_level_id")
	public PermissionLevel getPermissionLevel() {
		return permissionLevel;
	}

	public void setPermissionLevel(PermissionLevel permissionLevel) {
		this.permissionLevel = permissionLevel;
	}

	
	@Transient
	public Contact getClient() {
		return client;
	}

	public void setClient(Contact client) {
		this.client = client;
	}
	
	
	
	
	
	
	

}

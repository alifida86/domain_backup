package com.rocketcase.clioclient.domain;
// default package

// Generated Apr 28, 2017 6:29:29 PM by Hibernate Tools 5.1.0.Alpha1

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.rocketcase.clioclient.domain.base.BaseEntity;

/**
 * AclUsers
 */
@Entity
@Table(name = "acl_user_applications")
public class UserApplication implements BaseEntity, java.io.Serializable {

	@Transient
	public static final String SINGLE = "userApp";

	@Transient
	public static final String URL_PRE_FIX = "userapp";

	@Transient
	public static final String LIST_URL = URL_PRE_FIX + "/list";
	@Transient
	public static final String GET_BY_ID_URL = URL_PRE_FIX + "/id/";
	@Transient
	public static final String SAVE_URL = URL_PRE_FIX + "/save";
	@Transient
	public static final String DELETE_URL = URL_PRE_FIX + "/delete/";

	/**
	 * 
	 */
	private static final long serialVersionUID = -1611945140787694550L;
	private Long id;
	private User user;
	private Application application;

	public UserApplication() {
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "user_id")
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "application_id")
	public Application getApplication() {
		return application;
	}

	public void setApplication(Application application) {
		this.application = application;
	}

}

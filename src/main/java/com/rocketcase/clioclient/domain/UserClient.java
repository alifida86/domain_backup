package com.rocketcase.clioclient.domain;
// default package

// Generated Apr 28, 2017 6:29:29 PM by Hibernate Tools 5.1.0.Alpha1

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.rocketcase.clioclient.domain.base.BaseEntity;
import com.rocketcase.clioclient.domain.clio.apiv4.Contact;

@Entity
@Table(name = "user_clients")
public class UserClient implements BaseEntity, java.io.Serializable {

 

	private static final long serialVersionUID = -82781471853301912L;
	private Long id;
	private Long userId;
	private Long clientId;
	private Contact client;
	private User user;

	public UserClient() {
	}
	public UserClient(Long userId, Long clientId) {
		this.userId = userId;
		this.client = new Contact(clientId);
		this.clientId = clientId;
	}

	public UserClient(User user, Contact client) {
		this.user = user;
		this.client = client;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "client_id")
	public Contact getClient() {
		return client;
	}

	public void setClient(Contact client) {
		this.client = client;
	}
	
	
	 @Column(name = "user_id")
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	
	@Transient
	public Long getClientId() {
		return clientId;
	}
	public void setClientId(Long clientId) {
		this.clientId = clientId;
	}
	
	@Transient
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	
	
	
	
	
	
	 
}

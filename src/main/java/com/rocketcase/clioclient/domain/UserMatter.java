package com.rocketcase.clioclient.domain;
// default package

// Generated Apr 28, 2017 6:29:29 PM by Hibernate Tools 5.1.0.Alpha1

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.rocketcase.clioclient.domain.base.BaseEntity;
import com.rocketcase.clioclient.domain.clio.apiv4.Matter;

@Entity
@Table(name = "user_matters")
public class UserMatter implements BaseEntity, java.io.Serializable {

 

	private static final long serialVersionUID = -82781471853301912L;
	private Long id;
	private Long userId;
	private Long matterId;
	private Matter matter;
	private User user;

	public UserMatter() {
	}
	public UserMatter(Long userId, Long matterId) {
		this.userId = userId;
		this.matter = new Matter(matterId);
		this.matterId = matterId;
	}

	public UserMatter(User user, Matter matter) {
		this.user = user;
		this.matter = matter;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "matter_id")
	public Matter getMatter() {
		return matter;
	}

	public void setMatter(Matter matter) {
		this.matter = matter;
	}
	
	
	 @Column(name = "user_id")
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	
	@Transient
	public Long getMatterId() {
		return matterId;
	}
	public void setMatterId(Long matterId) {
		this.matterId = matterId;
	}
	
	@Transient
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	
	
	
	
	
	
	 
}

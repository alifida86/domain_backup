package com.rocketcase.clioclient.domain.clio.apiv4;

import java.lang.reflect.Field;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity
@Table(name = "clio_documents")
public class Document implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String API_URL = "documents";
	public static final String MULTIPLE_KEY = "documents";
	public static final String SINGLE_KEY = "document";

	public static final String FILE_FIELD = "document_version[uploaded_data]";

	@Expose
	@SerializedName("id")
	private Long id;

	private String encId;

	@Expose
	@SerializedName("matter")
	private Matter matter;

	@Expose
	@SerializedName("created_at")
	private String createdAt;

	@Expose
	@SerializedName("updated_at")
	private String updatedAt;

	@Expose
	@SerializedName("type")
	private String type;

	@Expose
	@SerializedName("filename")
	private String filename;

	private String status;
	
	
	public Document() {
	}

	public Document(Long id) {
		this.id = id;
	}

	public Document(Long id, String filename) {
		this.id = id;
		this.filename = filename;
	}

	public Document(Long id, Matter matter, String createdAt, String updatedAt, String type, String filename) {
		this.id = id;
		this.matter = matter;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
		this.filename = filename;
	}

	@Id

	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "matter_id")
	public Matter getMatter() {
		return this.matter;
	}

	public void setMatter(Matter matter) {
		this.matter = matter;
	}

	@Column(name = "created_at")
	public String getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	@Column(name = "updated_at")
	public String getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}

	@Column(name = "filename")
	public String getFilename() {
		return this.filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}
	
	@Column(name = "custom_status")
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	
	public static String getExposedFields(Boolean includeChildrens) {
		StringBuilder exposedFields = new StringBuilder();
		Field[] fileds = Document.class.getDeclaredFields();
		boolean firstTime = true;
		for (Field field : fileds) {
			SerializedName annotation = field.getAnnotation(SerializedName.class);
			if (annotation != null) {
				if (!firstTime) {
					exposedFields.append(",");
				}
				firstTime = false;
				exposedFields.append(annotation.value());
				if (includeChildrens) {
					if ("matters".equals(annotation.value())) {
						exposedFields.append("{");
						exposedFields.append(Matter.getExposedFields(false));
						exposedFields.append("}");
					}
					 
				}
			}
		}

		return exposedFields.toString();

	}
	
}
package com.rocketcase.clioclient.domain.clio.apiv4;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity
@Table(name = "clio_addresses")
public class Address implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3172673520666470644L;

	@Expose
	@SerializedName("id")
	private Long id;

	@Expose
	@SerializedName("name")
	private String name;

	@Expose
	@SerializedName("street")
	private String street;

	@Expose
	@SerializedName("city")
	private String city;

	@Expose
	@SerializedName("postal_code")
	private String postalCode;

	@Expose
	@SerializedName("province")
	private String province;

	@Expose
	@SerializedName("country")
	private String country;

	@Expose
	@SerializedName("created_at")
	private Date createdAt;

	@Expose
	@SerializedName("updated_at")
	private Date updatedAt;

	private Long contactId;

	private Contact contact;

	public Address() {
	}

	public Address(Long id) {
		this.id = id;
	}

	public Address(Long id, Contact contact, String name, String street, String city, String postalCode, String province, String country, Date createdAt, Date updatedAt) {
		this.id = id;
		this.contact = contact;
		this.name = name;
		this.street = street;
		this.city = city;
		this.postalCode = postalCode;
		this.province = province;
		this.country = country;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
	}

	@Id

	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	/*
	 * @ManyToOne(fetch = FetchType.LAZY)
	 * 
	 * @JoinColumn(name = "clio_contact_id")
	 */
	@Transient
	public Contact getContact() {
		return this.contact;
	}

	public void setContact(Contact contact) {
		this.contact = contact;
	}

	@Column(name = "name")
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "street")
	public String getStreet() {
		return this.street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	@Column(name = "city", length = 100)
	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	@Column(name = "postal_code", length = 20)
	public String getPostalCode() {
		return this.postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	@Column(name = "province", length = 100)
	public String getProvince() {
		return this.province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	@Column(name = "country", length = 100)
	public String getCountry() {
		return this.country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_at", length = 0)
	public Date getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_at", length = 0)
	public Date getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	@Column(name = "clio_contact_id")
	public Long getContactId() {
		return contactId;
	}

	public void setContactId(Long contactId) {
		this.contactId = contactId;
	}

}

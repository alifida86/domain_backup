package com.rocketcase.clioclient.domain.clio.apiv4;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity
@Table(name = "clio_matters")
public class Matter implements Serializable {

	public static final String API_URL = "matters";
	public static final String MULTIPLE_KEY = "matters";
	public static final String SINGLE_KEY = "matter";
	private static final long serialVersionUID = 1L;

	@Expose
	@SerializedName("id")
	private Long id;

	@Expose
	@SerializedName("client")
	private Contact client;


	@Expose
	@SerializedName("practice_area")
	private PracticeArea practiceArea;

	private String practiceAreaName;
	
	
	@Expose
	@SerializedName("responsible_attorney")
	private Attorney responsibleAttorney;
	private String responsibleAttorneyName;
	
	@Expose
	@SerializedName("originating_attorney")
	private Attorney originatingAttorney;
	private String originatingAttorneyName;

	@Expose
	@SerializedName("etag")
	private String etag;

	@Expose
	@SerializedName("custom_rate")
	private CustomRate customRate;

	@Expose
	@SerializedName("display_number")
	private String displayNumber;

	@Expose
	@SerializedName("description")
	private String description;

	@Expose
	@SerializedName("status")
	private String status;

	@Expose
	@SerializedName("client_reference")
	private String clientReference;

	@Expose
	@SerializedName("billable")
	private String billable;
	@Expose
	@SerializedName("maildrop_address")
	private String maildropAddress;
	@Expose
	@SerializedName("billing_method")
	private String billingMethod;
	@Expose
	@SerializedName("open_date")
	private Date openDate;
	@Expose
	@SerializedName("close_date")
	private Date closeDate;
	@Expose
	@SerializedName("pending_date")
	private Date pendingDate;
	@Expose
	@SerializedName("created_at")
	private String createdAt;
	@Expose
	@SerializedName("updated_at")
	private String updatedAt;
	@Expose
	@SerializedName("shared")
	private String shared;

	@Expose
	@SerializedName("contingency_fee")
	private ContingencyFee contingencyFee;

	@Expose
	@SerializedName("custom_field_values")
	private List<CustomFieldValue> customFieldValues = new ArrayList<CustomFieldValue>(0);

	private String customFieldUniqueID;
	private String licenseStatus;

	private List<ActivityRate> activityRates = new ArrayList<ActivityRate>(0);
	private List<Activity> activities = new ArrayList<Activity>();

	private List<Bill> bills = new ArrayList<Bill>();
	private List<Communication> communications = new ArrayList<Communication>(0);
	private List<Document> documents = new ArrayList<Document>(0);
	private Transaction transaction;
	private String location;

	private String customRateJson;
	private Double customAwardAmount;
	
	
	private List<Long> userIds;
	
	public Matter() {
	}

	public Matter(Long id) {
		this.id = id;
	}

	public Matter(Long id, String displayNumber) {
		this.id = id;
		this.displayNumber = displayNumber;
	}

	public Matter(Long id, String displayNumber, String description, String cfVal, String licenseStatus, String status, Contact client) {
		this.id = id;
		this.displayNumber = displayNumber;
		this.description = description;
		this.customFieldUniqueID = cfVal;
		this.licenseStatus = licenseStatus;
		this.status = status;
		this.client = client;
	}
	public Matter(Long id, String displayNumber, String description,  String licenseStatus, String status, Contact client) {
		this.id = id;
		this.displayNumber = displayNumber;
		this.description = description;
		this.licenseStatus = licenseStatus;
		this.status = status;
		this.client = client;
	}
	/*
	 * public Matter(Long id, String displayNumber, String cfVal) { this.id =
	 * id; this.displayNumber = cfVal + " - " + displayNumber; }
	 */

	public Matter(Long id, String displayNumber, String description, String status, Date openDate, Date closeDate, Date pendingDate, String location, String clientReference, String billable, String maildropAddress, String createdAt, String updatedAt, String billingMethod) {
		this.id = id;
		this.displayNumber = displayNumber;
		this.description = description;
		this.status = status;
		this.openDate = openDate;
		this.closeDate = closeDate;
		this.pendingDate = pendingDate;
		this.location = location;
		this.clientReference = clientReference;

		this.billable = billable;
		this.maildropAddress = maildropAddress;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
		this.billingMethod = billingMethod;
	}

	@Id
	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "display_number")
	public String getDisplayNumber() {
		return this.displayNumber;
	}

	public void setDisplayNumber(String displayNumber) {
		this.displayNumber = displayNumber;
	}

	@Column(name = "description")
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Column(name = "status")
	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "open_date")
	public Date getOpenDate() {
		return this.openDate;
	}

	public void setOpenDate(Date openDate) {
		this.openDate = openDate;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "close_date", length = 0)
	public Date getCloseDate() {
		return this.closeDate;
	}

	public void setCloseDate(Date closeDate) {
		this.closeDate = closeDate;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "pending_date")
	public Date getPendingDate() {
		return this.pendingDate;
	}

	public void setPendingDate(Date pendingDate) {
		this.pendingDate = pendingDate;
	}

	@Column(name = "location")
	public String getLocation() {
		return this.location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	@Column(name = "client_reference")
	public String getClientReference() {
		return this.clientReference;
	}

	public void setClientReference(String clientReference) {
		this.clientReference = clientReference;
	}

	@Column(name = "billable")
	public String getBillable() {
		return this.billable;
	}

	public void setBillable(String billable) {
		this.billable = billable;
	}

	@Column(name = "maildrop_address")
	public String getMaildropAddress() {
		return this.maildropAddress;
	}

	public void setMaildropAddress(String maildropAddress) {
		this.maildropAddress = maildropAddress;
	}

	@Column(name = "created_at")
	public String getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	@Column(name = "updated_at")
	public String getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}

	@Column(name = "billing_method")
	public String getBillingMethod() {
		return this.billingMethod;
	}

	public void setBillingMethod(String billingMethod) {
		this.billingMethod = billingMethod;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "client_id")
	public Contact getClient() {

		return client;
	}

	public void setClient(Contact client) {
		this.client = client;
	}

	@Column(name = "license_status")
	public String getLicenseStatus() {
		return licenseStatus;
	}

	public void setLicenseStatus(String licenseStatus) {
		this.licenseStatus = licenseStatus;
	}

	
	
	@Column(name = "custom_rate")
	public String getCustomRateJson() {
		if(customRate != null){
			this.customRateJson = new Gson().toJson(customRate);
		}
		return customRateJson;
	}

	public void setCustomRateJson(String customRateJson) {
		this.customRateJson = customRateJson;
	}

	
	
	
	 
	
	
	
	
	@Transient
	public ContingencyFee getContingencyFee() {
		return contingencyFee;
	}

	public void setContingencyFee(ContingencyFee contingencyFee) {
		this.contingencyFee = contingencyFee;
	}

	@Column(name = "responsible_attorney")
	public String getResponsibleAttorneyName() {
		return responsibleAttorneyName;
	}

	public void setResponsibleAttorneyName(String responsibleAttorneyName) {
		
		this.responsibleAttorneyName = responsibleAttorneyName;
	}

	
	
	
	
	
	
	
	
	
	@Transient
	public Attorney getOriginatingAttorney() {
		return originatingAttorney;
	}

	public void setOriginatingAttorney(Attorney originatingAttorney) {
		if(originatingAttorney != null && originatingAttorney.getName() != null) {
			this.originatingAttorneyName = originatingAttorney.getName() ;
		}
		this.originatingAttorney = originatingAttorney;
	}
	
	
	@Column(name = "originating_attorney")
	public String getOriginatingAttorneyName() {
		return originatingAttorneyName;
	}

	public void setOriginatingAttorneyName(String originatingAttorneyName) {
		this.originatingAttorneyName = originatingAttorneyName;
	}

	@Transient
	public PracticeArea getPracticeArea() {
		return practiceArea;
	}

	public void setPracticeArea(PracticeArea practiceArea) {
		if(practiceArea != null && practiceArea.getName() != null) {
			this.practiceAreaName = practiceArea.getName() ;
		}
		this.practiceArea = practiceArea;
	}

	@Column(name = "practice_area")
	public String getPracticeAreaName() {
		return practiceAreaName;
	}

	public void setPracticeAreaName(String practiceAreaName) {
		
		this.practiceAreaName = practiceAreaName;
	}

	@Transient
	public Attorney getResponsibleAttorney() {
		return responsibleAttorney;
	}

	public void setResponsibleAttorney(Attorney responsibleAttorney) {
		if(responsibleAttorney != null && responsibleAttorney.getName() != null) {
			this.responsibleAttorneyName = responsibleAttorney.getName() ;
		}
		this.responsibleAttorney = responsibleAttorney;
	}

	
	@Transient
	public List<ActivityRate> getActivityRates() {
		return activityRates;
	}

	public void setActivityRates(List<ActivityRate> activityRates) {
		this.activityRates = activityRates;
	}

	@Transient
	public List<Activity> getActivities() {
		return activities;
	}

	public void setActivities(List<Activity> activities) {
		this.activities = activities;
	}

	@Transient
	public List<Bill> getBills() {
		return bills;
	}

	public void setBills(List<Bill> bills) {
		this.bills = bills;
	}

	@Transient
	public List<CustomFieldValue> getCustomFieldValues() {
		return customFieldValues;
	}

	public void setCustomFieldValues(List<CustomFieldValue> customFieldValues) {
		this.customFieldValues = customFieldValues;
	}

	@Transient
	public Transaction getTransaction() {
		return transaction;
	}

	public void setTransaction(Transaction transaction) {
		this.transaction = transaction;
	}

	@Transient
	public String getCustomFieldUniqueID() {
		return customFieldUniqueID;
	}

	public void setCustomFieldUniqueID(String customFieldUniqueID) {
		this.customFieldUniqueID = customFieldUniqueID;
	}

	@Transient
	public List<Communication> getCommunications() {
		return communications;
	}

	public void setCommunications(List<Communication> communications) {
		this.communications = communications;
	}

	@Transient
	public List<Document> getDocuments() {
		return documents;
	}

	public void setDocuments(List<Document> documents) {
		this.documents = documents;
	}

	@Transient
	public CustomRate getCustomRate() {
		return customRate;
	}

	public void setCustomRate(CustomRate customRate) {
		this.customRate = customRate;
	}

	
	@Transient
	public Double getCustomAwardAmount() {
		this.customAwardAmount = 0D;
		if(this.customRateJson !=null){
			CustomRate customRate = new Gson().fromJson(this.customRateJson, CustomRate.class);
			if(customRate != null && customRate.getRates() != null){
				for (Rate rate : customRate.getRates()) {
					if(rate.getAward() != null){
						customAwardAmount += rate.getAward();
					}
					
				}
			}
		}
		
		return customAwardAmount;
	}

	public void setCustomAwardAmount(Double customAwardAmount) {
		this.customAwardAmount = customAwardAmount;
	}
 
	
	@Transient
	public List<Long> getUserIds() {
		return userIds;
	}

	public void setUserIds(List<Long> userIds) {
		this.userIds = userIds;
	}
	
	
	

	

	public static String getExposedFields(Boolean includeChildrens) {
		StringBuilder exposedFields = new StringBuilder();
		Field[] fileds = Matter.class.getDeclaredFields();
		boolean firstTime = true;
		for (Field field : fileds) {
			SerializedName annotation = field.getAnnotation(SerializedName.class);
			if (annotation != null) {

				if (!firstTime) {
					exposedFields.append(",");
				}
				firstTime = false;
				exposedFields.append(annotation.value());
				if (includeChildrens) {
					if ("client".equals(annotation.value())) {
						exposedFields.append("{");
						exposedFields.append(Contact.getExposedFields(false));
						exposedFields.append("}");
					}

					if ("custom_field_values".equals(annotation.value())) {
						exposedFields.append("{");
						exposedFields.append(CustomFieldValue.getExposedFields(false));
						exposedFields.append("}");
					}

					if ("contingency_fee".equals(annotation.value())) {
						exposedFields.append("{");
						exposedFields.append(ContingencyFee.getExposedFields(false));
						exposedFields.append("}");

					}
				}
			}
		}

		return exposedFields.toString();

	}

	

	
}

package com.rocketcase.clioclient.domain.clio.apiv4;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity
@Table(name = "clio_bills")
public class Bill implements java.io.Serializable {

	public static final String API_URL = "bills";
	public static final String MULTIPLE_KEY = "bills";
	public static final String SINGLE_KEY = "bill";

	
	private static final long serialVersionUID = 1L;

	@Expose
	@SerializedName("id")
	private Long id;

	private String encId;
	
	@Expose
	@SerializedName("client")
	private Contact contact;

	@Expose
	@SerializedName("type")
	private String type;

	

	@Expose
	@SerializedName("created_at")
	private String createdAt;

	@Expose
	@SerializedName("updated_at")
	private String updatedAt;

	//@Expose
	//@SerializedName("pdf_url")

	@Expose
	@SerializedName("number")
	private String number;

	@Expose
	@SerializedName("subject")
	private String subject;

	/*@Expose
	@SerializedName("currency")*/
	private String currency;

	@Expose
	@SerializedName("purchase_order")
	private String purchaseOrder;

	@Expose
	@SerializedName("memo")
	private String memo;

	@Expose
	@SerializedName("start_at")
	private Date startAt;

	@Expose
	@SerializedName("end_at")
	private Date endAt;

	@Expose
	@SerializedName("issued_at")
	private Date issuedAt;

	@Expose
	@SerializedName("due_at")
	private Date dueAt;

	@Expose
	@SerializedName("tax_rate")
	private Double taxRate;

	@Expose
	@SerializedName("secondary_tax_rate")
	private Double secondaryTaxRate;

	/*@Expose
	@SerializedName("discount")*/
	private Double discount;

	/*@Expose
	@SerializedName("discount_type")*/
	private String discountType;

	/*@Expose
	@SerializedName("discount_note")*/
	private String discountNote;

	@Expose
	@SerializedName("balance")
	private Double balance;

	/*@Expose
	@SerializedName("balance_with_interest")*/
	private Double balanceWithInterest;

	@Expose
	@SerializedName("total")
	private Double total;

	/*@Expose
	@SerializedName("total_with_interest")*/
	private Double totalWithInterest;

	/*@Expose
	@SerializedName("status")*/
	private String status;

	@Expose
	@SerializedName("state")
	private String state;

	@Expose
	@SerializedName("matters")
	private List<Matter> matters = new ArrayList<Matter>(0);

	public Bill() {
	}

	public Bill(long id) {
		this.id = id;
	}

	public Bill(long id, Contact contact, String type, String createdAt, String updatedAt,
			String number, String subject, String currency, String purchaseOrder, String memo,
			Date startAt, Date endAt, Date issuedAt, Date dueAt, Double taxRate, Double secondaryTaxRate,
			Double discount, String discountType, String discountNote, Double balance, Double balanceWithInterest,
			Double total, Double totalWithInterest, String status, String state) {
		this.id = id;
		this.contact = contact;
		this.type = type;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
		this.number = number;
		this.subject = subject;
		this.currency = currency;
		this.purchaseOrder = purchaseOrder;
		this.memo = memo;
		this.startAt = startAt;
		this.endAt = endAt;
		this.issuedAt = issuedAt;
		this.dueAt = dueAt;
		this.taxRate = taxRate;
		this.secondaryTaxRate = secondaryTaxRate;
		this.discount = discount;
		this.discountType = discountType;
		this.discountNote = discountNote;
		this.balance = balance;
		this.balanceWithInterest = balanceWithInterest;
		this.total = total;
		this.totalWithInterest = totalWithInterest;
		this.status = status;
		this.state = state;
	}

	@Id

	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "contact_id")
	public Contact getContact() {
		return this.contact;
	}

	public void setContact(Contact contact) {
		this.contact = contact;
	}

	@Column(name = "type")
	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Column(name = "created_at")
	public String getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	@Column(name = "updated_at")
	public String getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}

	 

	@Column(name = "number")
	public String getNumber() {
		return this.number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	@Column(name = "subject")
	public String getSubject() {
		return this.subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	@Column(name = "currency")
	public String getCurrency() {
		return this.currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	@Column(name = "purchase_order")
	public String getPurchaseOrder() {
		return this.purchaseOrder;
	}

	public void setPurchaseOrder(String purchaseOrder) {
		this.purchaseOrder = purchaseOrder;
	}

	@Column(name = "memo")
	public String getMemo() {
		return this.memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "start_at")
	public Date getStartAt() {
		return this.startAt;
	}

	public void setStartAt(Date startAt) {
		this.startAt = startAt;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "end_at")
	public Date getEndAt() {
		return this.endAt;
	}

	public void setEndAt(Date endAt) {
		this.endAt = endAt;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "issued_at")
	public Date getIssuedAt() {
		return this.issuedAt;
	}

	public void setIssuedAt(Date issuedAt) {
		this.issuedAt = issuedAt;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "due_at")
	public Date getDueAt() {
		return this.dueAt;
	}

	public void setDueAt(Date dueAt) {
		this.dueAt = dueAt;
	}

	@Column(name = "tax_rate", precision = 17, scale = 17)
	public Double getTaxRate() {
		return this.taxRate;
	}

	public void setTaxRate(Double taxRate) {
		this.taxRate = taxRate;
	}

	@Column(name = "secondary_tax_rate", precision = 17, scale = 17)
	public Double getSecondaryTaxRate() {
		return this.secondaryTaxRate;
	}

	public void setSecondaryTaxRate(Double secondaryTaxRate) {
		this.secondaryTaxRate = secondaryTaxRate;
	}

	@Column(name = "discount", precision = 17, scale = 17)
	public Double getDiscount() {
		return this.discount;
	}

	public void setDiscount(Double discount) {
		this.discount = discount;
	}

	@Column(name = "discount_type")
	public String getDiscountType() {
		return this.discountType;
	}

	public void setDiscountType(String discountType) {
		this.discountType = discountType;
	}

	@Column(name = "discount_note")
	public String getDiscountNote() {
		return this.discountNote;
	}

	public void setDiscountNote(String discountNote) {
		this.discountNote = discountNote;
	}

	@Column(name = "balance", precision = 17, scale = 17)
	public Double getBalance() {
		return this.balance;
	}

	public void setBalance(Double balance) {
		this.balance = balance;
	}

	@Column(name = "balance_with_interest", precision = 17, scale = 17)
	public Double getBalanceWithInterest() {
		return this.balanceWithInterest;
	}

	public void setBalanceWithInterest(Double balanceWithInterest) {
		this.balanceWithInterest = balanceWithInterest;
	}

	@Column(name = "total", precision = 17, scale = 17)
	public Double getTotal() {
		return this.total;
	}

	public void setTotal(Double total) {
		this.total = total;
	}

	@Column(name = "total_with_interest", precision = 17, scale = 17)
	public Double getTotalWithInterest() {
		return this.totalWithInterest;
	}

	public void setTotalWithInterest(Double totalWithInterest) {
		this.totalWithInterest = totalWithInterest;
	}

	@Column(name = "status")
	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Column(name = "state")
	public String getState() {
		return this.state;
	}

	public void setState(String state) {
		this.state = state;
	}

	@Transient
	public List<Matter> getMatters() {
		return matters;
	}

	public void setMatters(List<Matter> matters) {
		this.matters = matters;
	}


	 
	public static String getExposedFields(Boolean includeChildrens) {
		StringBuilder exposedFields = new StringBuilder();
		Field[] fileds = Bill.class.getDeclaredFields();
		boolean firstTime = true;
		for (Field field : fileds) {
			SerializedName annotation = field.getAnnotation(SerializedName.class);
			if (annotation != null) {
				if (!firstTime) {
					exposedFields.append(",");
				}
				firstTime = false;
				exposedFields.append(annotation.value());
				if (includeChildrens) {
					if ("matters".equals(annotation.value())) {
						exposedFields.append("{");
						exposedFields.append(Matter.getExposedFields(false));
						exposedFields.append("}");
					}
					if ("client".equals(annotation.value())) {
						exposedFields.append("{");
						exposedFields.append(Contact.getExposedFields(false));
						exposedFields.append("}");
					}
				}
			}
		}

		return exposedFields.toString();

	}

}

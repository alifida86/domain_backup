package com.rocketcase.clioclient.domain.clio.apiv4;

import java.io.Serializable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CustomFieldPicklistOption implements Serializable {

	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	@Expose
	@SerializedName("id")
	private Long id;

	@Expose
	@SerializedName("name")
	private String name;

	
	
	
	
	
	
	
	public CustomFieldPicklistOption() {
		
	}
	public CustomFieldPicklistOption(Long id, String name) {
		super();
		this.id = id;
		this.name = name;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}

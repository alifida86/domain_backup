package com.rocketcase.clioclient.domain.clio.apiv4;

import java.lang.reflect.Field;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Calendar implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5296298361115966272L;
	public static final String API_URL = "calendars";
	public static final String MULTIPLE_KEY = "calendars";
	public static final String SINGLE_KEY = "calendar";

	@Expose
	@SerializedName("id")
	private Long id;

	@Expose
	@SerializedName("name")
	private String name;

	@Expose
	@SerializedName("type")
	private String type;

	@Expose
	@SerializedName("created_at")
	private String createdAt;

	@Expose
	@SerializedName("updated_at")
	private String updatedAt;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}

	public static String getExposedFields(Boolean includeChildrens) {
		StringBuilder exposedFields = new StringBuilder();
		Field[] fileds = Calendar.class.getDeclaredFields();
		boolean firstTime = true;
		for (Field field : fileds) {
			SerializedName annotation = field.getAnnotation(SerializedName.class);
			if (annotation != null) {
				if (!firstTime) {
					exposedFields.append(",");
				}
				firstTime = false;
				exposedFields.append(annotation.value());
			}
		}

		return exposedFields.toString();

	}

}

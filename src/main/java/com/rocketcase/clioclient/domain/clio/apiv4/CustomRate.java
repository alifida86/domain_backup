package com.rocketcase.clioclient.domain.clio.apiv4;

import java.io.Serializable;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CustomRate implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6256191950335236723L;

	@Expose
	@SerializedName("rates")
	private List<Rate> rates;

	public CustomRate() {
		super();
	}

	public List<Rate> getRates() {
		return rates;
	}

	public void setRates(List<Rate> rates) {
		this.rates = rates;
	}

}
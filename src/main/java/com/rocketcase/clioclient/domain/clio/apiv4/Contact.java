package com.rocketcase.clioclient.domain.clio.apiv4;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity
@Table(name = "clio_contacts")
public class Contact implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	public static final String MULTIPLE_KEY = "contacts";
	public static final String SINGLE_KEY = "contact";
	public static final String API_URL = "contacts";

	@Expose
	@SerializedName("id")
	private Long id;

	@Expose
	@SerializedName("type")
	private String type;

	@Expose
	@SerializedName("created_at")
	private String createdAt;

	@Expose
	@SerializedName("updated_at")
	private String updatedAt;

	@Expose
	@SerializedName("clio_connect_email")
	private String clioConnectEmail;

	@Expose
	@SerializedName("prefix")
	private String prefix;

	@Expose
	@SerializedName("name")
	private String name;

	@Expose
	@SerializedName("first_name")
	private String firstName;

	@Expose
	@SerializedName("last_name")
	private String lastName;

	@Expose
	@SerializedName("title")
	private String title;

	/*
	 * @Expose
	 * 
	 * @SerializedName("company")
	 */
	private String company;

	@Expose
	@SerializedName("email_addresses")
	private List<EmailAddress> emailAddresses = new ArrayList<EmailAddress>(0);

	/*
	 * @Expose
	 * 
	 * @SerializedName("activity_rates") private List<ActivityRate>
	 * activityRates = new ArrayList<ActivityRate>(0);
	 */

	private List<Matter> matters = new ArrayList<Matter>(0);

	public Contact() {
	}

	public Contact(Long id) {
		this.id = id;
	}

	public Contact(Long id, String type) {
		this.id = id;
		this.type = type;
	}

	public Contact(Long id, String type, String createdAt, String updatedAt, String clioConnectEmail, String prefix, String name, String firstName, String lastName, String title, String company) {
		this.id = id;
		this.type = type;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
		this.clioConnectEmail = clioConnectEmail;

		this.prefix = prefix;
		this.name = name;
		this.firstName = firstName;
		this.lastName = lastName;
		this.title = title;
		this.company = company;
	}

	@Id

	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "type")
	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	 
	@Column(name = "created_at")
	public String getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	@Column(name = "updated_at")
	public String getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}

	@Column(name = "clio_connect_email")
	public String getClioConnectEmail() {
		return this.clioConnectEmail;
	}

	public void setClioConnectEmail(String clioConnectEmail) {
		this.clioConnectEmail = clioConnectEmail;
	}

	@Column(name = "prefix")
	public String getPrefix() {
		return this.prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	@Column(name = "name")
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "first_name")
	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	@Column(name = "last_name")
	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@Column(name = "title")
	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Column(name = "company")
	public String getCompany() {
		return this.company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	@Transient
	public List<Matter> getMatters() {
		return matters;
	}

	public void setMatters(List<Matter> matters) {
		this.matters = matters;
	}

	@Transient
	public List<EmailAddress> getEmailAddresses() {
		return emailAddresses;
	}

	public void setEmailAddresses(List<EmailAddress> emailAddresses) {
		this.emailAddresses = emailAddresses;
	}

	public static String getExposedFields(Boolean includeChildrens) {
		StringBuilder exposedFields = new StringBuilder();
		Field[] fileds = Contact.class.getDeclaredFields();
		boolean firstTime = true;
		for (Field field : fileds) {
			SerializedName annotation = field.getAnnotation(SerializedName.class);
			if (annotation != null) {
				if (!firstTime) {
					exposedFields.append(",");
				}
				firstTime = false;
				exposedFields.append(annotation.value());
				
				if (includeChildrens) {
					if ("email_addresses".equals(annotation.value())) {
						exposedFields.append("{");
						exposedFields.append(EmailAddress.getExposedFields(false));
						exposedFields.append("}");
					}
				}
			}
		}

		return exposedFields.toString();

	}

}

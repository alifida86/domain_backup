package com.rocketcase.clioclient.domain.clio.apiv4;

import java.lang.reflect.Field;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity
@Table(name = "clio_activities")
public class Activity implements java.io.Serializable {

	private static final long serialVersionUID = 5578636842449530709L;

	public static final String API_URL = "activities";
	public static final String MULTIPLE_KEY = "activities";
	public static final String SINGLE_KEY = "activity";

	@Expose
	@SerializedName("id")
	private Long id;

	@Expose
	@SerializedName("matter")
	private Matter matter;

	@Expose
	@SerializedName("created_at")
	private String createdAt;

	@Expose
	@SerializedName("updated_at")
	private String updatedAt;

	@Expose
	@SerializedName("type")
	private String type;

	@Expose
	@SerializedName("date")
	private Date date;

	@Expose
	@SerializedName("quantity")
	private Double quantity;

	@Expose
	@SerializedName("price")
	private Double price;

	@Expose
	@SerializedName("total")
	private Double total;

	@Expose
	@SerializedName("note")
	private String note;

	private Long activityDescriptionId;

	private Long userId;

	@Expose
	@SerializedName("communication")
	private String communication;

	@Expose
	@SerializedName("billed")
	private Boolean billed;

	@Expose
	@SerializedName("bill")
	private Bill bill;

	@Expose
	@SerializedName("subject")
	private String subject;

	private String utbmsActivity;

	private String utbmsTask;

	private String utbmsExpense;

	public Activity() {
	}

	public Activity(Long id) {
		this.id = id;
	}

	public Activity(Long id, Matter matter, String createdAt, String updatedAt, String type, Date date, Double quantity,
			Double price, Double total, String note, Long activityDescriptionId, Long userId, String communication,
			Boolean billed, Bill bill, String subject, String utbmsActivity, String utbmsTask, String utbmsExpense) {
		this.id = id;
		this.matter = matter;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
		this.type = type;
		this.date = date;
		this.quantity = quantity;
		this.price = price;
		this.total = total;
		this.note = note;
		this.activityDescriptionId = activityDescriptionId;
		this.userId = userId;
		this.communication = communication;
		this.billed = billed;
		this.bill = bill;
		this.subject = subject;
		this.utbmsActivity = utbmsActivity;
		this.utbmsTask = utbmsTask;
		this.utbmsExpense = utbmsExpense;
	}

	@Id

	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "matter_id")
	public Matter getMatter() {
		return this.matter;
	}

	public void setMatter(Matter matter) {
		this.matter = matter;
	}

	@Column(name = "created_at")
	public String getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	@Column(name = "updated_at")
	public String getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}

	@Column(name = "type")
	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "date")
	public Date getDate() {
		return this.date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	@Column(name = "quantity", precision = 17, scale = 17)
	public Double getQuantity() {
		return this.quantity;
	}

	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}

	@Column(name = "price", precision = 17, scale = 17)
	public Double getPrice() {
		return this.price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	@Column(name = "total", precision = 17, scale = 17)
	public Double getTotal() {
		return this.total;
	}

	public void setTotal(Double total) {
		this.total = total;
	}

	@Column(name = "note")
	public String getNote() {
		return this.note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	@Column(name = "activity_description_id")
	public Long getActivityDescriptionId() {
		return this.activityDescriptionId;
	}

	public void setActivityDescriptionId(Long activityDescriptionId) {
		this.activityDescriptionId = activityDescriptionId;
	}

	@Column(name = "user_id")
	public Long getUserId() {
		return this.userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	@Column(name = "communication")
	public String getCommunication() {
		return this.communication;
	}

	public void setCommunication(String communication) {
		this.communication = communication;
	}

	@Column(name = "billed")
	public Boolean getBilled() {
		return this.billed;
	}

	public void setBilled(Boolean billed) {
		this.billed = billed;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "bill_id")
	public Bill getBill() {
		return this.bill;
	}

	public void setBill(Bill bill) {
		this.bill = bill;
	}

	@Column(name = "subject")
	public String getSubject() {
		return this.subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	@Column(name = "utbms_activity")
	public String getUtbmsActivity() {
		return this.utbmsActivity;
	}

	public void setUtbmsActivity(String utbmsActivity) {
		this.utbmsActivity = utbmsActivity;
	}

	@Column(name = "utbms_task")
	public String getUtbmsTask() {
		return this.utbmsTask;
	}

	public void setUtbmsTask(String utbmsTask) {
		this.utbmsTask = utbmsTask;
	}

	@Column(name = "utbms_expense")
	public String getUtbmsExpense() {
		return this.utbmsExpense;
	}

	public void setUtbmsExpense(String utbmsExpense) {
		this.utbmsExpense = utbmsExpense;
	}

	
	
	public static String getExposedFields(Boolean includeChildrens) {
		StringBuilder exposedFields = new StringBuilder();
		Field[] fileds = Activity.class.getDeclaredFields();
		boolean firstTime = true;
		for (Field field : fileds) {
			
			SerializedName annotation = field.getAnnotation(SerializedName.class);
			
			
			if (annotation != null) {
				 
				if (!firstTime) {
					exposedFields.append(",");
				}
				firstTime = false;
				exposedFields.append(annotation.value());
				if (includeChildrens) {
					if ("matter".equals(annotation.value())) {
						exposedFields.append("{");

						exposedFields.append(Matter.getExposedFields(false));
						exposedFields.append("}");
					}
					if ("bill".equals(annotation.value())) {
						exposedFields.append("{");
						exposedFields.append(Bill.getExposedFields(false));
						exposedFields.append("}");
					}
					/*if ("communication".equals(annotation.value())) {
						exposedFields.append("{");
						exposedFields.append(Communication.getExposedFields(false));
						exposedFields.append("}");
					}*/
				}
			}
		}

		return exposedFields.toString();

	}
}

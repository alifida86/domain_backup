package com.rocketcase.clioclient.domain.clio.apiv4;

import java.lang.reflect.Field;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity
@Table(name = "clio_email_addresses")
public class EmailAddress implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4090389726186559855L;

	@Expose
	@SerializedName("id")
	private Long id;

	@Expose
	@SerializedName("name")
	private String name;

	@Expose
	@SerializedName("address")
	private String address;

	@Expose
	@SerializedName("default_email")
	private String defaultEmail;

	@Expose
	@SerializedName("created_at")
	private Date createdAt;

	@Expose
	@SerializedName("updated_at")
	private Date updatedAt;

	private Contact contact;

	private Long contactId;

	public EmailAddress() {
	}

	public EmailAddress(Long id) {
		this.id = id;
	}

	public EmailAddress(Long id, Contact contact, EmailAddress emailAddress, String name, String address, String defaultEmail, Date createdAt, Date updatedAt, Set<EmailAddress> emailAddresses) {
		this.id = id;
		this.name = name;
		this.address = address;
		this.defaultEmail = defaultEmail;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
		this.contact = contact;
	}

	@Id

	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "name")
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "address")
	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@Column(name = "default_email")
	public String getDefaultEmail() {
		return this.defaultEmail;
	}

	public void setDefaultEmail(String defaultEmail) {
		this.defaultEmail = defaultEmail;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_at")
	public Date getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_at")
	public Date getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	/*
	 * @ManyToOne(fetch = FetchType.LAZY)
	 * 
	 * @JoinColumn(name = "clio_contact_id")
	 */
	@Transient
	public Contact getContact() {
		return contact;
	}

	public void setContact(Contact contact) {
		this.contact = contact;
	}

	@Column(name = "clio_contact_id")
	public Long getContactId() {
		return contactId;
	}

	public void setContactId(Long contactId) {
		this.contactId = contactId;
	}

	public static String getExposedFields(Boolean includeChildrens ) {
		StringBuilder exposedFields = new StringBuilder();
		Field[] fileds = EmailAddress.class.getDeclaredFields();
		boolean firstTime = true;
		for (Field field : fileds) {
			SerializedName annotation = field.getAnnotation(SerializedName.class);
			if (annotation != null) {
				if (!firstTime) {
					exposedFields.append(",");
				}
				firstTime = false;
				exposedFields.append(annotation.value());
			}
		}

		return exposedFields.toString();

	} 
}

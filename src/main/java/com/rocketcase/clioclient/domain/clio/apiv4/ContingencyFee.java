package com.rocketcase.clioclient.domain.clio.apiv4;
 
import java.lang.reflect.Field;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

 
@Entity
@Table(name = "clio_contingency_fees")
public class ContingencyFee implements java.io.Serializable {

	/**
	 * 
	 */
	
	public static final String API_URL = "contingency_fees";
	
	public static final String MULTIPLE_KEY = "contingency_fees";
	public static final String SINGLE_KEY = "contingency_fee";
	
	private static final long serialVersionUID = 6406303138732150364L;
	
	@Expose
	@SerializedName("id")
	private Long id;
	
	@Expose
	@SerializedName("bill")
	private Bill bill;
	
	@Expose
	@SerializedName("matter")
	private Matter matter;
	
	@Expose
	@SerializedName("created_at")
	private Date createdAt;
	
	@Expose
	@SerializedName("updated_at")
	private Date updatedAt;
	
	@Expose
	@SerializedName("rate")
	private Double rate;
	
	@Expose
	@SerializedName("award")
	private Double award;
	
	@Expose
	@SerializedName("fee")
	private Double fee;
	
	@Expose
	@SerializedName("user_id")
	private Long userId;
	
	@Expose
	@SerializedName("activity_id")
	private Long activityId;
	
	@Expose
	@SerializedName("note")
	private String note;
	
	@Expose
	@SerializedName("date")
	private Date date;
	
	@Expose
	@SerializedName("billed")
	private Boolean billed;

	
	
	public ContingencyFee() {
	}

	public ContingencyFee(Long id) {
		this.id = id;
	}

	public ContingencyFee(Long id, Bill bill, Matter matter, Date createdAt, Date updatedAt,
			Double rate, Double award, Double fee, Long userId, Long activityId, String note, Date date,
			Boolean billed) {
		this.id = id;
		this.bill = bill;
		this.matter = matter;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
		this.rate = rate;
		this.award = award;
		this.fee = fee;
		this.userId = userId;
		this.activityId = activityId;
		this.note = note;
		this.date = date;
		this.billed = billed;
	}

	@Id

	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "bill_id")
	public Bill getBill() {
		return this.bill;
	}

	public void setBill(Bill bill) {
		this.bill = bill;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "matter_id")
	public Matter getMatter() {
		return this.matter;
	}

	public void setMatter(Matter matter) {
		this.matter = matter;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_at")
	public Date getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_at")
	public Date getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	@Column(name = "rate")
	public Double getRate() {
		return this.rate;
	}

	public void setRate(Double rate) {
		this.rate = rate;
	}

	@Column(name = "award")
	public Double getAward() {
		return this.award;
	}

	public void setAward(Double award) {
		this.award = award;
	}

	@Column(name = "fee")
	public Double getFee() {
		return this.fee;
	}

	public void setFee(Double fee) {
		this.fee = fee;
	}

	@Column(name = "user_id")
	public Long getUserId() {
		return this.userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	@Column(name = "activity_id")
	public Long getActivityId() {
		return this.activityId;
	}

	public void setActivityId(Long activityId) {
		this.activityId = activityId;
	}

	@Column(name = "note")
	public String getNote() {
		return this.note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "date")
	public Date getDate() {
		return this.date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	@Column(name = "billed")
	public Boolean getBilled() {
		return this.billed;
	}

	public void setBilled(Boolean billed) {
		this.billed = billed;
	}

	
	
	public static String getExposedFields(Boolean includeChildrens) {
		StringBuilder exposedFields = new StringBuilder();
		Field[] fileds = ContingencyFee.class.getDeclaredFields();
		boolean firstTime = true;
		for (Field field : fileds) {
			SerializedName annotation = field.getAnnotation(SerializedName.class);
			if (annotation != null) {
				
				if ("bill".equals(annotation.value())) {
					continue;
				}
				if ("matter".equals(annotation.value())) {
					continue;
				}
				if ("award".equals(annotation.value())) {
					continue;
				}
				if ("rate".equals(annotation.value())) {
					continue;
				}
				if ("fee".equals(annotation.value())) {
					continue;
				}
				if ("user_id".equals(annotation.value())) {
					continue;
				}
				if ("activity_id".equals(annotation.value())) {
					continue;
				}
				if ("date".equals(annotation.value())) {
					continue;
				}
				if ("note".equals(annotation.value())) {
					continue;
				}
				
				 
				
				if ("billed".equals(annotation.value())) {
					continue;
				}
				
				if ("created_at".equals(annotation.value())) {
					continue;
				}
				
				if ("updated_at".equals(annotation.value())) {
					continue;
				}
				
				
				
				if (!firstTime) {
					exposedFields.append(",");
				}
				firstTime = false;
				exposedFields.append(annotation.value());
				if (includeChildrens) {
					 
					if ("bill".equals(annotation.value())) {
						exposedFields.append("{");
						exposedFields.append(Bill.getExposedFields(false));
						exposedFields.append("}");
					}
					if ("matter".equals(annotation.value())) {
						exposedFields.append("{");
						exposedFields.append(Matter.getExposedFields(false));
						exposedFields.append("}");
					}
				}
			}
		}

		return exposedFields.toString();

	}
	
}

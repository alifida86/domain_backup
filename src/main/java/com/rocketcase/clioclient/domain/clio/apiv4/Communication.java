package com.rocketcase.clioclient.domain.clio.apiv4;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity
@Table(name = "clio_communications")
public class Communication implements java.io.Serializable {

	private static final long serialVersionUID = -964744711956176925L;

	public static final String API_URL = "communications";
	public static final String MULTIPLE_KEY = "communications";
	public static final String SINGLE_KEY = "communication";


	public static final String EMAIL_TYPE="EmailCommunication";
	
	

	@Expose
	@SerializedName("id")
	private Long id;

	@Expose
	@SerializedName("matter")
	private com.rocketcase.clioclient.domain.clio.apiv4.Matter matter;

	@Expose
	@SerializedName("type")
	private String type;

	@Expose
	@SerializedName("created_at")
	private String createdAt;

	@Expose
	@SerializedName("updated_at")
	private String updatedAt;

	@Expose
	@SerializedName("subject")
	private String subject;

	@Expose
	@SerializedName("body")
	private String body;

	private String status;

	@Expose
	@SerializedName("date")
	private Date date;

	/*@Expose
	@SerializedName("activities")*/
	private List<Activity> activities = new ArrayList<Activity>(0);

	@Expose
	@SerializedName("receivers")
	private List<com.rocketcase.clioclient.domain.clio.apiv4.Contact> receivers = new ArrayList<com.rocketcase.clioclient.domain.clio.apiv4.Contact>(0);

	@Expose
	@SerializedName("senders")
	private List<com.rocketcase.clioclient.domain.clio.apiv4.Contact> senders = new ArrayList<com.rocketcase.clioclient.domain.clio.apiv4.Contact>(0);

	public Communication() {
	}

	public Communication(Long id) {
		this.id = id;
	}
	public Communication(Long id, String subject, String body) {
		this.id = id;
		this.subject = subject;
		this.body = body;
	}

	public Communication(Long id, com.rocketcase.clioclient.domain.clio.apiv4.Matter matter, String type, String createdAt, String updatedAt, String subject, String body, Date date, String status) {
		this.id = id;
		this.matter = matter;
		this.type = type;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
		this.subject = subject;
		this.body = body;
		this.date = date;
		this.status = status;
	}

	@Id

	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "matter_id")
	public com.rocketcase.clioclient.domain.clio.apiv4.Matter getMatter() {
		return this.matter;
	}

	public void setMatter(com.rocketcase.clioclient.domain.clio.apiv4.Matter matter) {
		this.matter = matter;
	}

	@Column(name = "type")
	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Column(name = "created_at")
	public String getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	@Column(name = "updated_at")
	public String getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}

	@Column(name = "subject")
	public String getSubject() {
		return this.subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	@Column(name = "body")
	public String getBody() {
		return this.body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "date")
	public Date getDate() {
		return this.date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	@Column(name = "custom_status")
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Transient
	public List<Activity> getActivities() {
		return activities;
	}

	public void setActivities(List<Activity> activities) {
		this.activities = activities;
	}

	@Transient
	public List<com.rocketcase.clioclient.domain.clio.apiv4.Contact> getSenders() {
		return senders;
	}

	public void setSenders(List<com.rocketcase.clioclient.domain.clio.apiv4.Contact> senders) {
		this.senders = senders;
	}

	@Transient
	public List<com.rocketcase.clioclient.domain.clio.apiv4.Contact> getReceivers() {
		return receivers;
	}

	public void setReceivers(List<com.rocketcase.clioclient.domain.clio.apiv4.Contact> receivers) {
		this.receivers = receivers;
	}
	public static String getExposedFields(Boolean includeChildrens) {
		StringBuilder exposedFields = new StringBuilder();
		Field[] fileds = Communication.class.getDeclaredFields();
		boolean firstTime = true;
		for (Field field : fileds) {
			SerializedName annotation = field.getAnnotation(SerializedName.class);
			if (annotation != null) {
				if (!firstTime) {
					exposedFields.append(",");
				}
				firstTime = false;
				exposedFields.append(annotation.value());
				if (includeChildrens) {
					
					if("receivers".equals(annotation.value())){
						continue;
					}
					if("senders".equals(annotation.value())){
						continue;
					}
					/*if("matter".equals(annotation.value())){
						continue;
					}*/
					
					if ("receivers".equals(annotation.value())) {
						exposedFields.append("{");
						exposedFields.append(Contact.getExposedFields(false));
						exposedFields.append("}");
					}
					if ("senders".equals(annotation.value())) {
						exposedFields.append("{");
						exposedFields.append(Contact.getExposedFields(false));
						exposedFields.append("}");
					}
					if ("matter".equals(annotation.value())) {
						exposedFields.append("{");
						exposedFields.append(Matter.getExposedFields(false));
						exposedFields.append("}");
					}
				}
			}
		}

		return exposedFields.toString();

	}
}

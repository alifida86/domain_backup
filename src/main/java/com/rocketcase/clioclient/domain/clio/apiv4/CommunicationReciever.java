package com.rocketcase.clioclient.domain.clio.apiv4;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "clio_communications_recievers")
public class CommunicationReciever implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6308957705923133022L;
	private Long id;
	private Communication communication;
	private Contact contact;

	public CommunicationReciever() {
	}

	public CommunicationReciever(Long id) {
		this.id = id;
	}

	public CommunicationReciever(Long id, Communication communication, Contact contact) {
		this.id = id;
		this.communication = communication;
		this.contact = contact;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "communication_id")
	public Communication getCommunication() {
		return communication;
	}

	public void setCommunication(Communication communication) {
		this.communication = communication;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "contact_id")
	public Contact getContact() {
		return contact;
	}

	public void setContact(Contact contact) {
		this.contact = contact;
	}

}

package com.rocketcase.clioclient.domain.clio.apiv4;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity
@Table(name = "clio_custom_fields")
public class CustomField implements java.io.Serializable {

	private static final long serialVersionUID = -2914752130021493277L;
	
	
	
	

	@Expose
	@SerializedName("id")
	private Long id;

	@Expose
	@SerializedName("name")
	private String name;

	@Expose
	@SerializedName("parent_type")
	private String parentType;

	@Expose
	@SerializedName("field_type")
	private String fieldType;

	@Expose
	@SerializedName("displayed")
	private String displayed;

	@Expose
	@SerializedName("required")
	private String required;

	@Expose
	@SerializedName("deleted")
	private String deleted;

	@Expose
	@SerializedName("display_order")
	private Long displayOrder;

	@Expose
	@SerializedName("created_at")
	private Date createdAt;

	@Expose
	@SerializedName("updated_at")
	private Date updatedAt;

	public CustomField() {
	}

	public CustomField(Long id) {
		this.id = id;
	}

	public CustomField(Long id, String name, String parentType, String fieldType, String displayed, String required, String deleted, Long displayOrder, Date createdAt, Date updatedAt) {
		this.id = id;
		this.name = name;
		this.parentType = parentType;
		this.fieldType = fieldType;
		this.displayed = displayed;
		this.required = required;
		this.deleted = deleted;
		this.displayOrder = displayOrder;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
	}

	@Id

	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "name")
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "parent_type")
	public String getParentType() {
		return this.parentType;
	}

	public void setParentType(String parentType) {
		this.parentType = parentType;
	}

	@Column(name = "field_type")
	public String getFieldType() {
		return this.fieldType;
	}

	public void setFieldType(String fieldType) {
		this.fieldType = fieldType;
	}

	@Column(name = "displayed")
	public String getDisplayed() {
		return this.displayed;
	}

	public void setDisplayed(String displayed) {
		this.displayed = displayed;
	}

	@Column(name = "required")
	public String getRequired() {
		return this.required;
	}

	public void setRequired(String required) {
		this.required = required;
	}

	@Column(name = "deleted")
	public String getDeleted() {
		return this.deleted;
	}

	public void setDeleted(String deleted) {
		this.deleted = deleted;
	}

	@Column(name = "display_order")
	public Long getDisplayOrder() {
		return displayOrder;
	}

	public void setDisplayOrder(Long displayOrder) {
		this.displayOrder = displayOrder;
	}


	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_at")
	public Date getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_at")
	public Date getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

}

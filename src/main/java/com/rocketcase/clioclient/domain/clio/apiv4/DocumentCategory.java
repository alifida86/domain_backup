package com.rocketcase.clioclient.domain.clio.apiv4;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity
@Table(name = "clio_document_category")
public class DocumentCategory implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4090389726186559855L;

	@Expose
	@SerializedName("id")
	private Long id;

	@Expose
	@SerializedName("name")
	private String name;

	@Expose
	@SerializedName("url")
	private String url;

	
	public DocumentCategory() {
	}

	public DocumentCategory(Long id) {
		this.id = id;
	}

	public DocumentCategory(Long id, String name, String url) {
		this.id = id;
		this.name = name;
		this.url = url;
	}

	@Id

	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "name")
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "url")
	public String getUrl() {
		return this.url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

}

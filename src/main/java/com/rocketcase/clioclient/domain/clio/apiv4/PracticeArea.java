package com.rocketcase.clioclient.domain.clio.apiv4;

import java.io.Serializable;

public class PracticeArea implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7427727509075065882L;

	private Long id;
	private String name;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}

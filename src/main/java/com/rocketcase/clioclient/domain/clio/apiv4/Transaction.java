package com.rocketcase.clioclient.domain.clio.apiv4;

import static javax.persistence.GenerationType.IDENTITY;

import java.lang.reflect.Field;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity
@Table(name = "clio_transactions")
public class Transaction implements java.io.Serializable {

	private static final long serialVersionUID = -5810348472551036745L;

	public static final String API_URL = "bank_transactions";
	public static final String MULTIPLE_KEY = "bank_transactions";
	public static final String SINGLE_KEY = "bank_transaction";

	@Expose
	@SerializedName("id")
	private Long id;

	private Long customPk;

	@Expose
	@SerializedName("currency")
	private String currency;

	@Expose
	@SerializedName("source")
	private String source;

	@Expose
	@SerializedName("date")
	private Date updatedAt;

	@Expose
	@SerializedName("amount")
	private Double amount;

	private Long matterId;

	@Expose
	@SerializedName("matter")
	private Matter matter;

	@Expose
	@SerializedName("type")
	private String type;

	@Expose
	@SerializedName("confirmation")
	private String confirmation;

	@Expose
	@SerializedName("description")
	private String description;

	@Expose
	@SerializedName("transaction_type")
	private String transactionType;

	/*
	 * "id": 0, "type": "string", "etag": "string", "bank_account_id": 0,
	 * "source": "string", "confirmation": "string", "date": "2017-11-17",
	 * "amount": 0, "currency": "string", "description": "string",
	 * "exchange_rate": 0, "transaction_type": "strin
	 */

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "custom_pk")
	public Long getCustomPk() {
		return customPk;
	}

	public void setCustomPk(Long customPk) {
		this.customPk = customPk;
	}

	@Column(name = "id")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "currency")
	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	@Column(name = "date")
	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	@Column(name = "amount")
	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	@Column(name = "matter_id")
	public Long getMatterId() {
		return matterId;
	}

	public void setMatterId(Long matterId) {
		this.matterId = matterId;
	}

	@Column(name = "source")
	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	@Transient
	public Matter getMatter() {
		return matter;
	}

	public void setMatter(Matter matter) {
		this.matter = matter;

	}

	@Transient
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Transient
	public String getConfirmation() {
		return confirmation;
	}

	public void setConfirmation(String confirmation) {
		this.confirmation = confirmation;
	}

	@Transient
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Transient
	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public static String getExposedFields(Boolean includeChildrens) {
		StringBuilder exposedFields = new StringBuilder();
		Field[] fileds = Transaction.class.getDeclaredFields();
		boolean firstTime = true;
		for (Field field : fileds) {
			SerializedName annotation = field.getAnnotation(SerializedName.class);
			if (annotation != null) {
				if (!firstTime) {
					exposedFields.append(",");
				}
				firstTime = false;
				exposedFields.append(annotation.value());
			}
		}

		return exposedFields.toString();

	}

}

package com.rocketcase.clioclient.domain.clio.apiv4;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity
@Table(name = "clio_activity_rates")
public class ActivityRate implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8963844882817756274L;
	@Expose
	@SerializedName("id")
	private Long id;

	@Expose
	@SerializedName("rate")
	private Double rate;

	@Expose
	@SerializedName("flat_rate")
	private String flatRate;

	@Expose
	@SerializedName("activity_description")
	private String activityDescription;

	@Expose
	@SerializedName("created_at")
	private Date createdAt;

	@Expose
	@SerializedName("updated_at")
	private Date updatedAt;

	private Contact contact;

	private Long contactId;

	private Matter matter;

	private Long matterId;

	public ActivityRate() {
	}

	public ActivityRate(Long id) {
		this.id = id;
	}

	public ActivityRate(Long id, Contact contact, Matter matter, Double rate, String flatRate, String activityDescription, Date createdAt, Date updatedAt) {
		this.id = id;
		this.contact = contact;
		this.matter = matter;
		this.rate = rate;
		this.flatRate = flatRate;
		this.activityDescription = activityDescription;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
	}

	@Id

	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	/*
	 * @ManyToOne(fetch = FetchType.LAZY)
	 * 
	 * @JoinColumn(name = "clio_contact_id")
	 */
	@Transient
	public Contact getContact() {
		return this.contact;
	}

	public void setContact(Contact contact) {
		this.contact = contact;
	}

	/*
	 * @ManyToOne(fetch = FetchType.LAZY)
	 * 
	 * @JoinColumn(name = "clio_matter_id")
	 */
	@Transient
	public Matter getMatter() {
		return this.matter;
	}

	public void setMatter(Matter matter) {
		this.matter = matter;
	}

	@Column(name = "rate")
	public Double getRate() {
		return this.rate;
	}

	public void setRate(Double rate) {
		this.rate = rate;
	}

	@Column(name = "flat_rate")
	public String getFlatRate() {
		return this.flatRate;
	}

	public void setFlatRate(String flatRate) {
		this.flatRate = flatRate;
	}

	@Column(name = "activity_description")
	public String getActivityDescription() {
		return this.activityDescription;
	}

	public void setActivityDescription(String activityDescription) {
		this.activityDescription = activityDescription;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_at")
	public Date getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_at")
	public Date getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	@Column(name = "clio_contact_id")
	public Long getContactId() {
		return contactId;
	}

	public void setContactId(Long contactId) {
		this.contactId = contactId;
	}

	@Column(name = "clio_matter_id")
	public Long getMatterId() {
		return matterId;
	}

	public void setMatterId(Long matterId) {
		this.matterId = matterId;
	}

}

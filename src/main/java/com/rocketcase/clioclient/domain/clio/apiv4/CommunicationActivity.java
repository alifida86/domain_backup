package com.rocketcase.clioclient.domain.clio.apiv4;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "clio_communications_activities")
public class CommunicationActivity implements java.io.Serializable {



	private static final long serialVersionUID = -4204390249218897503L;
	private Long id;
	private Activity activity;
	private Communication communication;

	public CommunicationActivity() {
	}

	public CommunicationActivity(Long id, Activity activity, Communication communication) {
		this.id = id;
		this.activity = activity;
		this.communication = communication;
	}

	@Id
	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "activity_id", nullable = false)
	public Activity getActivity() {
		return activity;
	}

	public void setActivity(Activity activity) {
		this.activity = activity;
	}


	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "communication_id", nullable = false)
	public Communication getCommunication() {
		return communication;
	}

	public void setCommunication(Communication communication) {
		this.communication = communication;
	}

	

	
}

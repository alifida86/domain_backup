package com.rocketcase.clioclient.domain.clio.apiv4;


import java.lang.reflect.Field;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity
@Table(name = "clio_custom_field_values")
public class CustomFieldValue implements java.io.Serializable {



	
	
	private static final long serialVersionUID = -3447661971281789351L;
	
	
	@Expose
	@SerializedName("id")
	private String id;
	
	@Expose
	@SerializedName("matter")
	private Matter matter;
	
	
	
	@Expose
	@SerializedName("field_name")
	private String fieldName;
	
	@Expose
	@SerializedName("custom_field")
	private CustomField customField;
	
	@Expose
	@SerializedName("field_type")
	private String type;
	
	@Expose
	@SerializedName("value")
	private String value;
	
	@Expose
	@SerializedName("created_at")
	private Date createdAt;
	
	@Expose
	@SerializedName("updated_at")
	private Date updatedAt;

	
	@Expose
	@SerializedName("picklist_option")
	private CustomFieldPicklistOption customFieldPicklistOption;
	
	public CustomFieldValue() {
	}

	public CustomFieldValue(String id) {
		this.id = id;
	}

	public CustomFieldValue(String id, Matter matter, CustomField customField, String type,
			String value, Date createdAt, Date updatedAt) {
		this.id = id;
		this.matter = matter;
		this.customField = customField;
		this.type = type;
		this.value = value;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
	}

	@Id

	@Column(name = "id", unique = true, nullable = false)
	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "matter_id")
	public Matter getMatter() {
		return this.matter;
	}

	public void setMatter(Matter matter) {
		this.matter = matter;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "custom_field")
	public CustomField getCustomField() {
		return this.customField;
	}

	public void setCustomField(CustomField customField) {
		this.customField = customField;
	}

	@Column(name = "type")
	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	
	

	@Column(name = "field_name")
	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	@Column(name = "value")
	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_at")
	public Date getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_at")
	public Date getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	@Transient
	public CustomFieldPicklistOption getCustomFieldPicklistOption() {
		return customFieldPicklistOption;
	}

	public void setCustomFieldPicklistOption(CustomFieldPicklistOption customFieldPicklistOption) {
		this.customFieldPicklistOption = customFieldPicklistOption;
	}


	public static String getExposedFields(Boolean includeChildrens) {
		StringBuilder exposedFields = new StringBuilder();
		Field[] fileds = CustomFieldValue.class.getDeclaredFields();
		boolean firstTime = true;
		for (Field field : fileds) {
			SerializedName annotation = field.getAnnotation(SerializedName.class);
			if (annotation != null) {
				
				if("matter".equals(annotation.value())){
					continue;
				}
				/*if("custom_field".equals(annotation.value())){
					continue;
				}*/
				
				if (!firstTime) {
					exposedFields.append(",");
				}
				firstTime = false;
				exposedFields.append(annotation.value());
				/*if (includeChildrens) {
					if ("client".equals(annotation.value())) {
						exposedFields.append("{");

						exposedFields.append(Contact.getExposedFields(false));
						exposedFields.append("}");
					}
					 
				}*/	
			}
		}

		return exposedFields.toString();

	}
	

}

package com.rocketcase.clioclient.domain.clio.apiv4;

import java.io.Serializable;
import java.util.Date;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Rate implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -9081387401483894633L;

	@Expose
	@SerializedName("id")
	private Long id;

	@Expose
	@SerializedName("note")
	private String note;

	@Expose
	@SerializedName("award")
	private Double award;

	@Expose
	@SerializedName("date")
	private Date date;

	public Rate() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public Double getAward() {
		return award;
	}

	public void setAward(Double award) {
		this.award = award;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

}
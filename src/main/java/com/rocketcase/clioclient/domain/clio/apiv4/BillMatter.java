package com.rocketcase.clioclient.domain.clio.apiv4;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "clio_bills_matters")
public class BillMatter implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private Long id;
	private Bill bill;
	private Matter matter;

	public BillMatter() {
	}

	public BillMatter(Long id) {
		this.id = id;
	}

	public BillMatter(Long id, Bill bill, Matter matter) {
		this.id = id;
		this.bill = bill;
		this.matter = matter;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "bill_id")
	public Bill getBill() {
		return this.bill;
	}

	public void setBill(Bill bill) {
		this.bill = bill;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "matter_id")
	public Matter getMatter() {
		return this.matter;
	}

	public void setMatter(Matter matter) {
		this.matter = matter;
	}

}

package com.rocketcase.clioclient.domain;
// default package

import static javax.persistence.GenerationType.IDENTITY;

// Generated Apr 28, 2017 6:29:29 PM by Hibernate Tools 5.1.0.Alpha1

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.rocketcase.clioclient.domain.base.BaseEntity;

/**
 * AclGroups
 */
@Entity
@Table(name = "acl_groups")
public class Group implements BaseEntity, java.io.Serializable {

	@Transient
	public static final String SINGLE = "group";
	@Transient
	public static final String URL_PRE_FIX = "group";

	@Transient
	public static final String LIST_URL = URL_PRE_FIX + "/list";
	@Transient
	public static final String GET_BY_ID_URL = URL_PRE_FIX + "/id/";

	@Transient
	public static final String GET_BY_ALIAS_URL = URL_PRE_FIX + "/alias/";
	
	@Transient
	public static final String GET_FOR_USER_ACCOUNT_CREATION = URL_PRE_FIX + "/clientaccounts/";
	
	@Transient
	public static final String SAVE_URL = URL_PRE_FIX + "/save";
	@Transient
	public static final String DELETE_URL = URL_PRE_FIX + "/delete/";

	private static final long serialVersionUID = 8497100410786652209L;
	private Long id;
	private String name;
	private String alias;

	private List<Permission> permissions = new ArrayList<Permission>(0);

	private List<UserGroup> userGroups = new ArrayList<UserGroup>(0);

	public Group() {
	}

	public Group(Long id) {
		this.id = id;
	}

	public Group(String name) {
		this.name = name;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "name")
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "alias")
	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	@Transient
	public List<Permission> getPermissions() {
		return permissions;
	}

	public void setPermissions(List<Permission> permissions) {
		this.permissions = permissions;
	}

	@Transient
	public List<UserGroup> getUserGroups() {
		return userGroups;
	}

	public void setUserGroups(List<UserGroup> userGroups) {
		this.userGroups = userGroups;
	}

}

package com.rocketcase.clioclient.domain;
// default package

// Generated Apr 28, 2017 6:29:29 PM by Hibernate Tools 5.1.0.Alpha1

import static javax.persistence.GenerationType.IDENTITY;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.rocketcase.clioclient.domain.base.BaseEntity;

/**
 * AclActions
 */
@Entity
@Table(name = "acl_actions")
public class Action implements BaseEntity, java.io.Serializable {

	@Transient
	public static final String SINGLE = "action";
	@Transient
	public static final String URL_PRE_FIX = "action";

	@Transient
	public static final String LIST_URL = URL_PRE_FIX + "/list";
	@Transient
	public static final String GET_BY_ID_URL = URL_PRE_FIX + "/id/";
	@Transient
	public static final String SAVE_URL = URL_PRE_FIX + "/save";
	@Transient
	public static final String DELETE_URL = URL_PRE_FIX + "/delete/";

	private static final long serialVersionUID = 1L;
	private Long id;
	private String url;
	private String name;
	private String mappingKey;
	private Boolean isPublic;
	private Boolean isResource;

	private List<Permission> permissionses = new ArrayList<Permission>(0);

	public Action() {
	}

	public Action(String url, String name) {
		this.url = url;
		this.name = name;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "url", length = 65535)
	public String getUrl() {
		return this.url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@Column(name = "name", length = 100)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "is_public")
	public Boolean getIsPublic() {
		return isPublic;
	}

	public void setIsPublic(Boolean isPublic) {
		this.isPublic = isPublic;
	}

	@Column(name = "is_resource")
	public Boolean getIsResource() {
		return isResource;
	}

	public void setIsResource(Boolean isResource) {
		this.isResource = isResource;
	}

	@Column(name = "mapping_key", length = 100)
	public String getMappingKey() {
		return mappingKey;
	}

	public void setMappingKey(String mappingKey) {
		this.mappingKey = mappingKey;
	}

	@Transient
	public List<Permission> getPermissionses() {
		return permissionses;
	}

	public void setPermissionses(List<Permission> permissionses) {
		this.permissionses = permissionses;
	}

}
